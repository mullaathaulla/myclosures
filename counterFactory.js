function counterFactory(){
    let counterVariable=0;
    return {
        incr: function increment(){ return ++counterVariable},
        decr: function decrement(){ return --counterVariable}
    }
}

module.exports=counterFactory;
