function cacheFunction(cb){
    let cacheObject={};
    return function storeSquareOfNumber(array){
        let newArray=[];
        for(let x of array){
            if(x in cacheObject){
                newArray.push(cacheObject[x]);
            }
            else{
                cacheObject[x]=cb(x);
                newArray.push(cacheObject[x]);
            }
        }
       return newArray;
    }
}

module.exports=cacheFunction;
