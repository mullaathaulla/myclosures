countOfInnerFunctionInvokedCb=0;

function limitFunctionCallCount(cb,n){
        return function innerMost(){
            if(countOfInnerFunctionInvokedCb<n){
                cb();
                ++countOfInnerFunctionInvokedCb;
            }
            else{
                return "Count Limit Exceeded";
            }
            return `${countOfInnerFunctionInvokedCb} time executed`;
        }
}

module.exports=limitFunctionCallCount;
